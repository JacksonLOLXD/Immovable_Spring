/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.CityDAO;
import Models.City;
import java.util.ArrayList;  
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  

/**
 *
 * @author REDP
 */

@Controller
@RequestMapping("")
public class CityController  {
    @Autowired
    CityDAO dao;
    
    /*It displays a form to input data, here "command" is a reserved request attribute 
     *which is used to display object data into form 
     */  
    @RequestMapping("/empform")  
    public ModelAndView showform(){  
        return new ModelAndView("empform","command",new City());  
    }  
    /*It saves object into database. The @ModelAttribute puts request data 
     *  into model object. You need to mention RequestMethod.POST method  
     *  because default request is GET*/  
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public ModelAndView save(@ModelAttribute("emp") City ci){  
        dao.save(ci);  
        return new ModelAndView("redirect:/viewemp");//will redirect to viewemp request mapping  
    }  
    /* It provides list of employees in model object */  
    @RequestMapping("/viewemp")  
    public ModelAndView viewemp(){  
        List<City> list=dao.getCities();  
        return new ModelAndView("viewemp","list",list);  
    }  
    /* It displays object data into form for the given id.  
     * The @PathVariable puts URL data into variable.*/  
    @RequestMapping(value="/editemp/{id}")  
    public ModelAndView edit(@PathVariable int id){  
        City emp=dao.getCityById(id);  
        return new ModelAndView("empeditform","command",emp);  
    }  
    /* It updates model object. */  
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public ModelAndView editsave(@ModelAttribute("emp") City emp){  
        dao.update(emp);  
        return new ModelAndView("redirect:/viewemp");  
    }  
    /* It deletes record for the given id in URL and redirects to /viewemp */  
    @RequestMapping(value="/deleteemp/{id}",method = RequestMethod.GET)  
    public ModelAndView delete(@PathVariable int id){  
        dao.delete(id);  
        return new ModelAndView("redirect:/viewemp");  
    }
    /*
        UsuariosValidar usuariosValidar;
        private JdbcTemplate jdbcTemplate;

        public AddController() 
        {
            this.usuariosValidar=new UsuariosValidar();
            Conectar con=new Conectar();
            this.jdbcTemplate=new JdbcTemplate(con.conectar() );
        }
        @RequestMapping(method=RequestMethod.GET) 
        public ModelAndView form()
        {
            ModelAndView mav=new ModelAndView();
            mav.setViewName("add");
            mav.addObject("usuarios",new Usuarios());
            return mav;
        }
        @RequestMapping(method=RequestMethod.POST)
        public ModelAndView form
            (
                    @ModelAttribute("usuarios") Usuarios u,
                    BindingResult result,
                    SessionStatus status
            )
        {
            this.usuariosValidar.validate(u, result);
            if(result.hasErrors())
            {
                ModelAndView mav=new ModelAndView();
                mav.setViewName("add");
                mav.addObject("usuarios",new Usuarios());
                return mav;
            }else
            {
            this.jdbcTemplate.update
            (
            "insert into usuarios (nombre,correo,telefono ) values (?,?,?)",
             u.getNombre(),u.getCorreo(),u.getTelefono()
            );
             return new ModelAndView("redirect:/home.htm");
            }

        }
    */
}
