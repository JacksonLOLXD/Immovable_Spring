/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author REDP
 */
public class Conexion {
    public DriverManagerDataSource conectar(){
        DriverManagerDataSource data = new DriverManagerDataSource();
        data.setDriverClassName("com.mysql.jdbc.Driver");
        data.setUrl("jdbc:mysql://localhost/bd_inmmovable");
        data.setUsername("root");
        data.setPassword("");
        return data;
    }
}
