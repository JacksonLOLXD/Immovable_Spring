package DAO;

import Models.City;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;  
/**
 *
 * @author JacksonLOLXD
 */
public class CityDAO {

    JdbcTemplate template;
    
    public void setTemplate(JdbcTemplate template) {
        this.template = template;
    }
    
    public int save(City ci){
        String sql = "insert into city (name) values("+ci.getName()+")";
        return template.update(sql);
    }
    
    public int update(City ci){  
        String sql="update city set name='"+ci.getName()+"' where id_city="+ci.getId_city();  
        return template.update(sql);  
    }  
    public int delete(int id){  
        String sql="delete from city where id_city="+id;  
        return template.update(sql);  
    }  
    public City getCityById(int id){  
        String sql="select * from city where id_city=?";  
        return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<City>(City.class));  
    }  
    public List<City> getCities(){  
        return template.query("select * from city",new RowMapper<City>(){  
            public City mapRow(ResultSet rs, int row) throws SQLException {  
                City ci=new City();  
                ci.setId_city(rs.getInt(1));  
                ci.setName(rs.getString(2));   
                return ci;  
            }
        });
    }  
}
