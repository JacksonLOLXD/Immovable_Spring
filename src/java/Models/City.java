/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author REDP
 */
public class City {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_city() {
        return id_city;
    }

    public void setId_city(int id_city) {
        this.id_city = id_city;
    }

    public City(String name, int id_city) {
        this.name = name;
        this.id_city = id_city;
    }

    public City() {
    }
    
    private String name;
    private int id_city;
    
    
}
